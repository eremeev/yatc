$(function(){
	
	var Message = Backbone.Model.extend({});

	var Messages = Backbone.Collection.extend({
		model : Message
	});

	var User = Backbone.Model.extend({});

	var Users = Backbone.Collection.extend({
		model : User
	});
	
	var UserView = Backbone.View.extend({
		tagElement: 'div',
		render: function(){
			this.$el.html(this.model.get('user'));
			return this
		}
	});

	var UsersView = Backbone.View.extend({
		tagElement: 'span',
		initialize: function(){
			this.model.bind('reset', this.render, this);
		},
		render: function(){
			this.$el.html('');
			_.each(this.model.models, function(user){
				//console.log(user.get('user'));
				this.$el.append(new UserView({model:user}).render().$el)
			}, this)
			$('#poponline').attr('data-content',this.$el.html())
			$('#onlinecnt').html(this.model.models.length)
		}
	});

	var SendView = Backbone.View.extend({
		el: $('body'),
		events: {
			'click #submit' : 'send',
			'click #killall': 'killall',
			'keydown': 'press'
		},
		initialize: function(){
			this.refocus();
		},
		refocus: function(){
			$('#msg').val('');
			$('#msg').focus();
			$('#msg').autosize();
			$('#msg').trigger('autosize');
			$('#poponline').popover('hide')
		},
		send: function(){
			var _this = this;
			var message = $('#msg').val()
			if (message != '') {
				$.post('/message', {message:message}, function(){
					_this.refocus();
				});
			} else {
				_this.refocus();
			}
			
		},
		press: function(event){
			console.log(event)
			if (event.ctrlKey && event.which == 13) {			// Ctrl + Enter
				this.send()										// 		Отправка
			} else if (event.ctrlKey && event.which == 112) {	// Ctrl + F1
				this.killall()									// 		Палево
			} else if (event.ctrlKey && event.which == 81) {	// Ctrl + Q
				document.location.href = '/logout'				// 		Выход
			} else if (event.ctrlKey && event.which == 192) {	// Ctrl + ~
				$('#poponline').popover('toggle')				//		Кто онлайн?
			}
		},
		killall: function(){
			$.post('/message', {message:'clear'}, function(){
				//$('#msglist').html('');
			});
		}
	});

	var MessageView = Backbone.View.extend({
		tagName: 'pre',
		template : _.template($('#message_view').html()),
		render : function(){
			this.$el.html(this.template(this.model.attributes));
			return this;
		}
	});

	var MessagesView = Backbone.View.extend({
		el: $('#msglist'),
		cursor: 0,
		initialize: function(){
			this.model.bind('reset', this.render, this);
			this.model.bind('add', this.add, this);
			this.bind('polldone', this.poll, this);
			this.bind('pollerror', this.pollerror, this);
			this.poll();
		},
		render: function(){
			_.each(this.model.models, function(message){
				if (message.get('body') == 'clear'){
					$('#msglist').html('');
				}
				else{
					this.$el.append(new MessageView({model:message}).render().$el);
				}
			}, this);
		},
		add: function(message){
			this.$el.append(new MessageView({model:message}).render().$el);
		},
		poll: function() {
			var _this = this;
			$.get('/listen', {cursor:this.cursor}, function(data){
				users.reset(_.map(data.online, function(user){
					return new User({'user':user});
				}))

				mlst = _.map(data.messages, function(message){
					return new Message(message);
					
				});
				_this.model.reset(mlst);
				_this.cursor = _.last(_this.model.models).get('time');
				$(window).scrollTop($('#msg').position().top)
				_this.trigger('polldone');
			})
			.error(function(){
				setTimeout(function(){_this.poll()}, 2000);
			})
		},
	});

	var messages = new Messages;
	var users = new Users;
	var users_view = new UsersView({model:users});
	var messages_view = new MessagesView({model:messages});
	var send = new SendView;

});