#!/usr/bin/python
# -*- coding: utf-8 -*-

import tornado.ioloop
import tornado.web
import time
import tornado.httpserver
import tornado.ioloop
from tornado.options import define, options
import time
import json
import os.path
from redis import Redis

class Application(tornado.web.Application):
	def __init__(self):
		handlers = [
			(r'/', MainHandler),
			(r'/message', MessageHandler),
			(r'/listen', ListenHandler),
			(r'/stat', StatHandler),
			(r'/login', LoginHandler),
			(r'/logout', LogoutHandler)
		]
		settings = dict(
			cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
			login_url = '/login',
			static_path = os.path.join(os.path.dirname(__file__), "static"),
		)
		tornado.web.Application.__init__(self, handlers, **settings)

		self.users 	= []
		self.redis 	= Redis()
		self.key 	= 'chat:messages'
		self.userkey 	= 'chat:users'
		self.onlinekey  = 'chat:online:%s'

		keys = self.redis.keys(self.onlinekey % '*')
		for key in keys:
			self.redis.delete(key)

	def online(self):
		users = self.redis.keys(self.onlinekey % '*')
		users = [user.split(':')[-1:][0] for user in users]
		return users

class BaseHandler(tornado.web.RequestHandler):
	def get_current_user(self):
		return self.get_secure_cookie('user')


class MainHandler(BaseHandler):
	@tornado.web.authenticated
	def get(self):
		self.render('templates/main.html', name = self.get_secure_cookie('user'))


class MessageHandler(BaseHandler):
	@tornado.web.authenticated
	@tornado.web.asynchronous
	def post(self):
		localtime = time.time()
		message = dict(
			user = self.get_secure_cookie('user'),
			body = self.get_argument('message'),
			time = str(localtime)
		)
		if message['body'] == 'clear':
			self.application.redis.delete(self.application.key)
		while self.application.users:
			user = self.application.users.pop()
			res = {'messages':[message], 'online':self.application.online()}
			user.finish(res)
		
		self.application.redis.zadd(self.application.key, json.dumps(message), localtime)
		self.application.redis.zremrangebyrank(self.application.key, 0, -1001)
		self.finish('ok')


class ListenHandler(BaseHandler):
	@tornado.web.authenticated
	@tornado.web.asynchronous
	def get(self):
		cursor = self.get_argument('cursor', None)
		if cursor:
			cursor = float(cursor)
			messages = self.application.redis.zrangebyscore(self.application.key, cursor+0.01, '+inf')
		else:
			messages = self.application.redis.zrangebyscore(self.application.key, '-inf', '+inf')

		if messages:
			messages = [json.loads(message) for message in messages]
			self.finish({'messages':messages, 'online':self.application.online()})
		else:
			self.application.users.append(self)
			if self.application.redis.setnx(self.application.onlinekey % self.get_secure_cookie('user'),1):
				msg = {'user':'system', 'body':'пользователь %s вошел в чат' % self.get_secure_cookie('user'), 'time':time.time()}
				while self.application.users:
					user = self.application.users.pop()
					user.finish({'messages':[msg], 'online':self.application.online()})

	def on_connection_close(self):
		self.application.users.remove(self)
		self.application.redis.delete(self.application.onlinekey % self.get_secure_cookie('user'))
		msg = {'user':'system', 'body':'пользователь %s вышел из чата' % self.get_secure_cookie('user'), 'time':time.time()}
		while self.application.users:
			user = self.application.users.pop()
			user.finish({'messages':[msg], 'online':self.application.online()})



class StatHandler(BaseHandler):
	def get(self):
		online = [user.split(':')[-1:][0] for user in self.application.online()]
		self.write({'online':online})


class LoginHandler(BaseHandler):
	def get(self):
		self.render('templates/login.html')

	def post(self):
		try:
			user = self.get_argument('name')
			passwd = self.get_argument('password')
			remember = self.get_argument('remember', None)
			idf = '%s:%s' % (user, passwd)
			ismember = self.application.redis.sismember(self.application.userkey, '%s:%s' % (user, passwd))
			if ismember:
				if remember:
					self.set_secure_cookie('user', user)
				else:
					self.set_secure_cookie('user', user, expires_days = None)
			self.redirect('/')
		except:
			self.redirect('/login')

class LogoutHandler(BaseHandler):
	def get(self):
		self.clear_cookie("user")
		self.redirect('/')


if __name__ == '__main__':
	define('port', default=8888, help='listening port')
	define('address', default='localhost', help='bind address')
	tornado.options.parse_command_line()

	app = Application()
	app.listen(options.port, address=options.address)
	
	#data_dir = os.path.dirname(__file__)
	#s = tornado.httpserver.HTTPServer(app, ssl_options={
	#	"certfile": os.path.join(data_dir, "host.cert"),
	#	"keyfile": os.path.join(data_dir, "host.key"),
	#})
	#s.listen(8888)
	tornado.ioloop.IOLoop.instance().start()
